from setuptools import setup, find_packages

setup(
  name="myml",
  version='0.0.1',
  description="My personal python package for machine learning related stuff",
  url="git@gitlab.com:csauer/myml.git",
  author="Christof Sauer",
  author_email="csauer@cern.ch",
  license="unlicense",
  packages=find_packages(),
  include_package_data = True,
  package_data = {
    # If any package contains *.ini files, include them
    '': ['*.ini'],
  },
  zip_safe=False
)
