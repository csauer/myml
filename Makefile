init:
	pip install --user --upgrade .
	pip install -r install/requirements.txt

test:
	python test/test_install.py

.PHONY: init test
