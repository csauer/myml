import os, sys

# Get module path
script_path = os.path.join(os.path.dirname(__file__) , "..")
sys.path.append(script_path)
from myml.data.reader.root2seq import SeqDataFromROOT 

def _run ():

  # Run a quick test
  print("[INFO] Run a quick test")
  path2file = os.path.join(os.path.join(script_path, "test/data/seq_data.root"))
  reader = SeqDataFromROOT()
  reader.addVariable("px")
  reader.addVariable("py")
  reader.addVariable("pz")
  reader.addDataset(path2file, "EventTree", "own", cut="px>0")
  reader.prepareTrainAndTestTree(1000, 10, batch_size=1)
  reader.setProcessor(fname="config/scaler_root2seq")

  x, y, _ = reader.getBatch(verbose=2)
  print(x)
  x, y, _ = reader.getBatch(verbose=2)
  x, y, _ = reader.getBatch(verbose=2)


if __name__ == "__main__":

  _run()
