import os, sys
import numpy as np

# Get module path
script_path = os.path.join(os.path.dirname(__file__) , "..")
sys.path.append(script_path)
from myml.data.preparation import Scaler

def _run ():

  x = np.array([1,2,3,4,5,6,7,8,9])
  y = np.array([2,2,234,5,0,7,8,9])
  z = np.array([-1,1,3,0,5,1,2,8,9])

  scaler = Scaler(method="standard;min_max")
  scaler.fit(x, "x")
  scaler.fit(y, "y")
  scaler.fit(z, "z")
  print("Non-transformed data :")
  print(x)
  print("Transformed data :")
  print(scaler.transform(x))
  print("Inverse transformed data :")
  print(scaler.inverseTransform(scaler.transform(x)))
  # Save scaler
  scaler.save("config/scaler1")
  # Creat new scaler and load previous configuration
  scaler_new = Scaler("config/scaler1.json")
  print(scaler_new.transform(x))
  print("COMBINE METHODS")
  scaler = Scaler(method="min_max;standard")
  scaler.fit(x, "x")
  scaler.fit(y, "y")
  scaler.fit(z, "z")
  print("Non-transformed data :")
  print(x)
  print("Transformed data :")
  print(scaler.transform(x))
  print("Inverse transformed data :")
  print(scaler.inverseTransform(scaler.transform(x)))
# Save scaler
  scaler.save("config/scaler2")


if __name__ == "__main__":

  _run()
