
def hashing (key):

  for entry in hash_table:
    if key in hash_table[entry].keys():
      return hash_table[entry][key]
  return key

# Initialize hash table as dict
hash_table = {}

# Losses implemented
hash_table["loss"] = {"binary_crossentropy" : "bce"}

# All kind of optimizers
hash_table["opt"] = {"Adam": "adam", "RMSprop": "rmsprop"}
