import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, project_dir)


def get_parser (**kwargs):

  """
  General method to get argument parser for preprocessing scripts.

  Arguments:
      kwargs: Flags indicating which arguments should be used.
  """

  import argparse

  # Set some default variables
  get_parser.default_outdir = "OUTPUT"

  # Help calls
  help_input_directory = "Directory where input files are stored"
  help_training_inFile = "Training input file in HDF5 format"
  help_validation_inFile = "Validation input file in HDF5 format"
  help_testing_inFile = "Testing input file in HDF5 format"
  help_tagging_object = "Object to be tagged: top or W"
  help_architecture = "Whiche architecture to use for the model"
  help_training_input_branches = "Input branches to be used for training" 
  help_training_weight_branches = "Input branches to be used for testing"
  help_reload_nn = "Reload previously trained model, provide architecture (1st argument; JSON) and weights (2nd argument; HDF5) and epoch number of weights that will be used (3rd argument; epoch number) (4th argument: set to calculate the predictions on)(default: %(default)s)."
  help_restart_training = "Restart traiing. provide weights (1st argument; HDF5) and number of epochs to train over (originally) (2nd argument; epoch number) that will be used  (default: %(default)s)."
  help_batch_size = "Batch size: Set the number of jets to look before updating the weights of the NN (default: %(default)s)."
  help_batch_norm = "Use batch normalization for all layers."
  help_n_epoch = "Set the number of epochs to train over (default: %(default)s)."
  help_init_distr = "Weight initialization distribution determines how the initial weights are set (default: %(default)s)."
  help_seed_nr = "Seed initialization (default: %(default)s)."
  help_validation_set = "Part of the training set to be used as validation set (default: %(default)s)."
  help_model_option = "Architecture of the NN, options for different nodes per layer (default: %(default)s)."
  help_activation_function = "activation function (default: %(default)s)."
  help_learning_rate = "Learning rate used by the optimizer (default: %(default)s)."
  help_adam_beta1 = "Beta1 used by the Adam optimizer (default: %(default)s)."
  help_adam_beta2 = "Beta2 used by the Adam optimizer (default: %(default)s)."
  help_optimizer = "optimizer for training (default: %(default)s)."
  help_objective = "objective (or loss) function for training (default: %(default)s)."
  help_r_l1 = "l1 weight regularization penalty (default: %(default)s)."
  help_r_l2 = "l2 weight regularization penalty (default: %(default)s)."
  help_activity_r_l1 = "l1 activity regularization (default: %(default)s)."
  help_activity_r_l2 = "l2 activity regularization (default: %(default)s)."
  help_preprocc_type = "Different pre-processing options (default: %(default)s)."

  # List of possible arguments
  arguments = {
    # Data preparation
    "input-sig": \
      dict(action="store", dest="input_sig", required=True,
      help="Input directory from which to read input ROOT files with signal samples."),
    "input-bkg": \
      dict(action="store", dest="input_bkg", required=True,
      help="Input directory from which to read input ROOT file with background samples."),
    "output-dir": \
      dict(action="store", type=str, dest="output_dir", default=get_parser.default_outdir,
      help="Output directory, to which to write output files."),
    "cut-train": \
      dict(type=str, nargs="+",
      default=["fjet_m>40000", "fjet_numConstituents>=3", "fjet_Tau32_wta>=0", "fjet_D2<50"],
      help="Cuts used for training set."),
    "suffix": \
      dict(type=str, default="",
      help="Suffix appended to final file name."),
    "truth-label": \
      dict(type=str, default="rel20.7", choices=["rel20.7", "rel21.0", "josu", "incl", "split23varied"],
      help="Definition of the signal to be used."),
    "max-processes": \
      dict(action="store", type=int, default=5,
      help="Maximum number of concurrent processes to use."),
    "n-train": \
      dict(action="store", type=int, required=True,
      help="Size of training datasets in millions of events."),
    "start": \
      dict(action="store", type=int, default=0,
      help="Start index of array slicing"),
    "step": \
      dict(action="store", type=int, default=3,
      help="Step size in array slicing"),
    "stop": \
      dict(action="store", type=int, default=-1,
      help="Stop index in array slicing"),
    "slim-config": \
      dict(action="store", type=str, default=os.path.join(project_dir, "..", "etc/trimslim1.ini"),
      help="Configuration file with slimming conditions."),
    # Training
    "jc-config": \
      dict(action="store", type=str, default=os.path.join(project_dir, "..", "etc/jet_collections.ini"),
      help="Configuration file with definition of different jet sub structure variables."),
    "jssv-config": \
      dict(action="store", type=str, default=os.path.join(project_dir, "..", "etc/jet_ssv.ini"),
      help="Configuration file with definition of different jet sub structure variables."),
    "objective": \
      dict(type=str, default="binary_crossentropy",
      choices=["binary_crossentropy" , "categorical_crossentropy", "mse"]),
    "architecture": \
      dict(type=str, default="Dense",
      choices=["Dense"],
      help=help_architecture),
    "optimizer": \
      dict(type=str, default="adam",
      choices=["adam", "adamax", "rmsprop"],
      help=help_optimizer),
    "suffix-data": \
      dict(type=str, default="",
      help="Add a suffix to data section in model name"),
    "suffix-model": \
      dict(type=str, default="",
      help="Add a suffix to model section in model name"),
    "suffix-train": \
      dict(type=str, default="",
      help="Add a suffix to train section in model name"),
    "learning-rate": \
      dict(type=float, default = 0.00005,
      help=help_learning_rate),
    "adam-beta1": \
      dict(type=float, default=0.9,
      help=help_adam_beta1),
    "adam-beta2": \
      dict(type=float, default = 0.999,
      help=help_adam_beta2),
    "clipnorm": \
      dict(type=float, default=0.,
      help="clipnorm for gradient clipping (default: %(default)s)."),
    "clip-sample-weight": \
      dict(type=float, default=0,
      help="Clip upper value of sample weights."),
    "n-epoch": \
      dict(type=int, default=200,
      help=help_n_epoch),
    "pt-low": \
      dict(type=int, default=0,
      help="Lower pt cut on truth level pt."),
    "pt-high": \
      dict(type=int, default=10000*1000,
      help="Higher pt cut on truth level pt."),
    "flat-pt": \
      dict(type=str, default="fjet_truthJet_pt", choices=["fjet_truthJet_pt", "fjet_pt"],
      help="Pt variable that is suposed to be flat during training"),
    "mass-low": \
      dict(type=int, default=40*1000,
      help="Lower mass cut."),
    "seed-nr": \
      dict(type=int, default=12264,
      help=help_seed_nr),
    "batch-size": \
      dict(type=int, default=1000,
      help=help_batch_size),
    "n-bins-pt-flat": \
      dict(type=int, default=100,
      help="Number of bins to compute weights to flatten the pt spectrum"),
    "model-config": \
      dict(type=str, default="5Dense",
      choices=["1Dense", "2Dense", "3Dense", "4Dense", "5Dense", "6Dense", "8Dense"],
      help=help_model_option),
    "node-config": \
      dict(type=str, default="nNodesWrtFeatures1",
      choices=["nNodesWrtFeatures1", "nNodesWrtFeatures2", "nNodesWrtFeatures3"],
      help=help_model_option),
    "number-layers": \
      dict(type=int, default=5, choices=[7, 6, 5, 4, 3],
      help="number of hidden layers (default: %(default)s)."),
    "activation-function": \
      dict(type=str, default="relu", choices=["relu", "tanh", "ELU", "PReLU", "SReLU"],
      help=help_activation_function),
    "r-l1": \
      dict(type=float, default=0.001,
      help=help_r_l1),
    "r-l2": \
      dict(type=float, default=0.,
      help=help_r_l2),
    "patience": \
      dict(type=int, default=50,
      help="number of epochs witout improvement of loss before stopping"),
    "batch-norm": \
      dict(type=int, default=1,
      choices=[0, 1],
      help="normalize between layers for each batch"),
    "training-weight": \
      dict(type=str, default="weight_train",
      choices=["weight_train", "weight_adv", "", "no_weight"],
      help="train without sample weight"),
    "testing-weight": \
      dict(type=str, default="fjet_weight_train",
      choices=["fjet_weight_adv", "", "fjet_testing_weight_pt", "fjet_testing_weight_pt_dR", "fjet_weight_train"],
      help="Weights used for evaluation"),
    "LRS": \
      dict(action="store_true",
      help="use Learning Rate Scheduler"),
    "init-dist": \
      dict(type=str, default="glorot_uniform",
      help=help_init_distr),
    "save-best-NN-only": \
      dict(type=bool, default=False,
      help="Only the NN weights after each epoch if the validation loss decreased to a new minimum"),
    "reload-nn": \
      dict(type=str, nargs="+", default=["",""],
      help=help_reload_nn),
    "pt-binning": \
      dict(type=str, nargs="+", default=[350, 450, 550, 650, 750, 850, 950, 1050, 1200, 1350, 1500, 1650, 1800, 1950, 2100, 2300, 2500, 2700, 2900, 3150, 3400, 3700, 4000],
      help="Pt binning used for evaluation of the tagger."),
    "eta-binning": \
      dict(type=str, nargs="+", default=[0, 0.7, 1.4, 2.0],
      help="Eta binning used for evaluation of the tagger."),
    "restart-training": \
      dict(type=str, nargs="+", default=["",""],
      help=help_restart_training),
    "preprocc-type-option": \
      dict(type=str, default="standard",
      choices=["mean_rms", "min_max", "standard"],
      help=help_preprocc_type),
    "input-group-name": \
      dict(type=str, default="group11",
      help="inputs_group_name to keep track of the training inputs"),
    # Add prediction branch
    "inputs": \
      dict(type=str, nargs="+", required=True,
      help="List of inputs."),
    "input": \
      dict(action="store", type=str, required=True,
      help="Path to project/input."),
    "treename": \
      dict(action="store", type=str, required=False, default="FlatSubstructureJetTree",
      help="Name of the TTree."),
    "text-on-plot": \
      dict(action="store", type=str, default="",
      help="Some additional information to add tp plot."),
    # Plotting
    "divide": \
      dict(type=str, nargs="+", required=True,
      help="List of inputs in numerator."),
    "by": \
      dict(action="store", type=str, required=True,
      help="Path to project in the denominator."),
    "entries": \
      dict(type=str, nargs="+", required=False,
      help="Entries for legend."),
    "training-data": \
      dict(action="store", type=str, required=True,
      help="Path to training data HDF5 file.")}

  # Validate
  kwargs = {k.replace("_","-"): v for (k,v) in kwargs.iteritems()}
  for k in set(kwargs) - set(arguments):
    raise IOError("get_parser: [ERROR] Keyword %s is not supported." % k)

  # Construct parser
  parser = argparse.ArgumentParser(description="[Generic] Argiment parser.")
  for k in filter(lambda k: kwargs[k], kwargs):
    parser.add_argument("--" + k, **arguments[k])

  return parser
