import os, sys

# Help calls
help_n_train = "Size of training dataset"
help_n_test = "Size of testing dataset"
help_objective ="Which function to minimize"
help_suffix_train = "Add a suffix to training section in model name"
help_suffix_data = "Add a suffix to data section in model name"
help_suffix_model = "Add a suffix to model section in model name"
help_batch_size = "Batch size: Set the number of jets to look before updating the weights of the NN (default: %(default)s)."
help_n_epoch = "Set the number of epochs to train over (default: %(default)s)."
help_seed_nr = "Seed initialization (default: %(default)s)."
help_learning_rate = "Learning rate used by the optimizer (default: %(default)s)."
help_adam_beta1 = "Beta1 used by the Adam optimizer (default: %(default)s)."
help_adam_beta2 = "Beta2 used by the Adam optimizer (default: %(default)s)."
help_optimizer = "optimizer for training (default: %(default)s)."
help_objective = "objective (or loss) function for training (default: %(default)s)."
help_preprocc_type = "Different pre-processing options (default: %(default)s)."

# Defaults
def_objective = "binary_crossentropy"
def_optimizer = "adam"
def_suffix_data = ""
def_suffix_train = ""
def_suffix_model = ""
def_learning_rate = 0.00005
def_preprocc_type = "standard"
def_adam_beta1 = 0.9
def_adam_beta2 = 0.999
def_n_epoch = 200
def_sed_nr = 12264
def_batch_size = 32

# Choices
choices_objective = ["binary_crossentropy" , "categorical_crossentropy", "mse"]
choices_optimizer = ["adam", "adamax", "rmsprop"]
choices_preprocc_type = ["min_max", "standard"]

# List of possible arguments
arguments = {
  "n-train": \
    dict(action="store", type=int, required=True,
    help=help_n_train),
  "n-test": \
    dict(action="store", type=int, required=True,
    help=help_n_test),
  "objective": \
    dict(type=str, default=def_objective,
    choices=choices_objective,
    help=help_objective),
  "optimizer": \
    dict(type=str, default=def_optimizer,
    choices=choices_optimizer,
    help=help_optimizer),
  "suffix-data": \
    dict(type=str, default=def_suffix_data,
    help=help_suffix_train),
  "suffix-model": \
    dict(type=str, default=def_suffix_model,
    help=help_suffix_model),
  "suffix-train": \
    dict(type=str, default=def_suffix_train,
    help=help_suffix_data),
  "learning-rate": \
    dict(type=float, default = def_learning_rate,
    help=help_learning_rate),
  "adam-beta1": \
    dict(type=float, default=def_adam_beta1,
    help=help_adam_beta1),
  "adam-beta2": \
    dict(type=float, default = def_adam_beta2,
    help=help_adam_beta2),
  "n-epoch": \
    dict(type=int, default=def_n_epoch,
    help=help_n_epoch),
  "seed-nr": \
    dict(type=int, default=def_sed_nr,
    help=help_seed_nr),
  "batch-size": \
    dict(type=int, default=def_batch_size,
    help=help_batch_size),
  "preprocc-type-option": \
    dict(type=str, default=def_preprocc_type,
    choices=choices_preprocc_type,
    help=help_preprocc_type)
}
