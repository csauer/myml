import numpy as np


def train_on_batch (model, x, y=None, sample_weight=None, class_weight=None, reset_metrics=True, return_dict=False, shuffle=True):

  if shuffle:
    # Get array of permuted indices
    if isinstance(x, list):
      idx = np.random.permutation(len(x[0]))
    else:
      idx = np.random.permutation(len(x))

    # -- X
    if isinstance(x, list):
      for i in range(len(x)):
        x[i] = x[i][idx]
    else:
      x = x[idx]
    # -- Y
    if y is not None:
      if isinstance(y, list):
        for i in range(len(x)):
          y[i] = y[i][idx]
      else:
        y = y[idx]
    # -- W
    if sample_weight is not None:
      if isinstance(sample_weight, dict):
        for key in sample_weight:
          sample_weight[key] = sample_weight[key][idx]
      else:
        sample_weight = sample_weight[idx]

  # Train model
  return model.train_on_batch (x, y=y, sample_weight=sample_weight, class_weight=class_weight, reset_metrics=reset_metrics)
