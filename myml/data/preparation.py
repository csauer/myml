import os, sys
import errno
import json
import numpy as np


def shuffle_array (*arg):

  # Check if all arrays have the same length
  if not all(len(arr)==len(args[0]) for arr in args):
    print("[ERROR] All arrays must have the same length")
    sys.exit()

  # Get index array
  idx = numpy.random.permutation(len(args))

  # Update arrays
  for i, arr in enumerate(args): args[i] = args[idx]

  return args


def offset(x):

  return np.negative(np.nanmean(x))


def scale(x):

  return np.reciprocal(np.nanstd(x))


# Warning: All parameter identifiers must be unique!
__pp_dict__ = {
  "id" :  {
    "par": {"dummy":  1},
    "lambda": "lambda x, inverse=False: x if not inverse else x",
    "method":"id"},
  "min_max" : {
    "par": {"min":np.nanmin,  "max":np.nanmax},
    "lambda": "lambda x, inverse=False: (x-__min__)/(__max__-__min__) if not inverse else x*(__max__-__min__)+__min__",
    "method":"min_max"},
  "standard" : {
    "par": {"offset": offset, "scale":scale},
    "lambda": "lambda x, inverse=False: __scale__*(x+__offset__) if not inverse else x/__scale__-__offset__",
    "method":"standard"},
  "image_scaler" : {
    "par": {"denominator": 255.0},
    "lambda": "lambda x, inverse=False: x/__denominator__ if not inverse else x*__denominator__",
    "method":"image_scaler"}
  }


class Scaler(object): 


  def __init__(self, fname=None, method="id", method_delimiter=";", verbose=True):

    """
    Scaler class data preprocessing

    :param fname: File name of the configuration file
    :param method: Preprocessing method. Choose between [...]
    'param method_delimiter: delimiter that separates different processing methods
    :type fname: str
    :type method: str
    :type method_delimiter: str
    """

    # Set the name of the final (or loaded) configuration file
    self.fname = fname
    self.method_delimiter = method_delimiter 
    # The `checkname` dictionary keeps track of names that appear more than once
    self.checkname = {}
    self.loaded = False
    self.p = []
    self.freezed = False
    # Set the preprocessing method (default is `id`, i.e., do nothing)
    if not isinstance(method, list):
      self.method = method.split(self.method_delimiter)

    # Some quick checks
    assert any(item in __pp_dict__.keys() for item in self.method), "[ERROR] The requested method is not supported"

    # If no file provided...
    if fname == None: return

    # Check if the file name points to a valid configuration file
    self.load(self.fname, verbose)

    if not verbose: return

    # Else, initialize parameter dict based on arguments
    if len(self.method) == 1 and self.method[0] == "id":
      print("[INFO] This scaler does nothing; it's the identity function")
    else:
      if len(self.method) > 1:
        print("[INFO] This preprocessing is a combination of several functions:")
        for name in self.item("name"):
          print("       |- %s - f(x) := (%s)(x)" % (name, "*".join(self.varDict(name)["method"])))


  def fit(self, data, name="x", method=None):

    if self.freezed:
      print("[INFO] Freez has been called; this dataset will not be fitted")
      return

    if self.loaded:
      print("[INFO] Using parameters of loaded file")
      return

    if method != None:
      print("[INFO] Preprocessing method of choice for %s: %s" % (name, method))
      if not isinstance(method, list):
        method = method.split(self.method_delimiter)
    else:
      method = self.method
    # Check if name already exists
    if name in self.item("name"):
      return
#      if name not in self.checkname:
#        self.checkname[name] = 1
#      else:
#        self.checkname[name] += 1
#      name = "%s_%s" % (name, self.checkname[name])
#      print("[WARNING] All names must be uniqe. Using `%s` instead" % name)
 
    # Create a new dictionary associated with this variable
    self.var = {"name":name, "fitted": {m : False for m in method}}
    # Add the parameters
    for m in method:
      for par_name in __pp_dict__[m]["par"]:
        self.var[par_name] = None
    # Add the method
    self.var["method"] = [m for m in method]
    # Add to list of parameter dictionaries
    self.p.append(self.var)
    # Start fitting
    self.partialFit(data, name)


  def freez(self):

    self.freezed = True


  def partialFit(self, data, name):

    if self.freezed:
      print("[INFO] Freez has been called; this dataset will not be fitted")
      return
   
    # Get processing dictionary associated with this name
    var_dict = self.varDict(name)

    # Make a copy of the non-transformed data
    _data = data

    # Get parameters required for this method
    print("[INFO] Computed the following parameters for preprocessing (%s):" % name)
    for method in var_dict["method"]:
      par_dict = __pp_dict__[method]["par"]
      for key in par_dict:
        # Check if the value is a callable function
        if hasattr(par_dict[key], "__call__"):
          var_dict[key] = float(par_dict[key](_data))
        # If not, it's just a scaler
        else:
          var_dict[key] = float(par_dict[key])
        # Some output
        if len(var_dict["method"]) == 1:
          print("       |- %s = %s" % (key, var_dict[key]))
        else:
          # Underline current method
          methods = list(var_dict["method"])
          methods[methods.index(method)] = "\033[4m%s\033[0m" % method
          print("       |- (%s) %s = %s" % ("*".join(methods), key, var_dict[key]))
      # Done
      var_dict["fitted"][method] = True
      # Update the dictionary for this variable
      self.p[self.item("name").index(name)] = var_dict
      # Transform the data
      if len(var_dict["method"]) > 1:
        _data = self.transform(_data, name)


  def save(self, fname=None):

    # Some checks
    if fname == None:
      fname = self.fname
      if self.loaded:
        print("[WARNING] the previous scaler will be overwritten!")
    elif fname != None and self.fname != None and fname != self.fname:
      print("[WARNING] Name conflict. Two names have been provided: `%s` and `%s`. Using the first one." % (fname, self.fname))
    # Check if all data has been fitted. If so, remove keyword
    for name in self.item("name"):
      var_dict = self.varDict(name)
      if not all( var_dict["fitted"][m] for m in var_dict["fitted"]):
        print("[ERROR] Scaler has not be saved since not all parameters have been fitted to data.")
        sys.exit()
      del self.p[self.item("name").index(name)]["fitted"]

    # Create directory
    if not os.path.exists(os.path.dirname(fname)) and os.path.dirname(fname) != "":
      try:
        os.makedirs(os.path.dirname(fname))
      except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
          raise 

    # Save data in json format
    if not fname.endswith(".json"): fname += ".json"
    json.dump(self.p, open( fname, "w" ))
    print("[INFO] Scaler has been saved to: %s" % fname)

    # Add fitted keyword again
    for name in self.item("name"):
      var_dict = self.varDict(name)
      var_dict["fitted"] = { m : True for m in var_dict["method"]}
      self.p[self.item("name").index(name)] = var_dict


  def load(self, fname, verbose=True):

    if not fname.endswith(".json"): fname += ".json"

    if os.path.isfile(fname):
      # Load previous fit
      self.p = json.load(open(fname, "r" ))
      self.method = self.item("method")
      self.loaded = True
      for name in self.item("name"):
        var_dict = self.varDict(name)
        var_dict["fitted"] = { m : True for m in var_dict["method"]}
        self.p[self.item("name").index(name)] = var_dict
      # Since this data is loaded, add fitting keyword
      if verbose:
        print("[INFO] The following parameter file has been loaded to scaler: %s" % fname)


  def transform(self, data, name="x", exclude=None):

    if name not in self.item("name"): return data

    # Get dictionary for this variable
    var_dict = self.p[self.item("name").index(name)]

    # Get a copy of the data before any transformation has been applied
    _data = data

    # Get the lambda function for transformation
    for method in var_dict["method"]:
      if not var_dict["fitted"][method]:
        continue
      f_str = __pp_dict__[method]["lambda"]
      # Set parameter
      for par_name in var_dict:
        if "__%s__" % par_name not in f_str:
          continue
        f_str = f_str.replace("__%s__" % par_name, str(var_dict[par_name]))
      # Evaluate string expression to get lambda function
      f_lam = eval(f_str)
      # Apply
      if exclude is not None:
        msk = _data != exclude
        _data_msk = _data[msk]
        _data_transformed_msk = f_lam(x=_data_msk, inverse=False)
        _data[msk] = _data_transformed_msk
      else:
        _data = f_lam(x=_data, inverse=False)

    return _data


  def item(self, name):
 
    return [item[name] for item in self.p]


  def isFitted(self):

    fitted = [fitted for item in self.p for fitted in item["fitted"]]
    if len(fitted) == 0:
      return False
    else:
      return all(fitted)


  def varDict(self, name):

    return self.p[self.item("name").index(name)]


  def inverseTransform(self, data, name="x"):

    if name not in self.item("name"): return data

    # Get dictionary for this variable
    var_dict = self.p[self.item("name").index(name)]

    # Get a copy of the data before any transformation has been applied
    _data = data

    # Get the lambda function for transformation
    for method in var_dict["method"][::-1]:
      f_str = __pp_dict__[method]["lambda"]
      # Set parameter
      for par_name in var_dict:
        if "__%s__" % par_name not in f_str:
          continue
        f_str = f_str.replace("__%s__" % par_name, str(var_dict[par_name]))
      # Evaluate string expression to get lambda function that will be applied to data
      f_lam = eval(f_str)
      # Apply
      _data = f_lam(x=_data, inverse=True)

    return _data


