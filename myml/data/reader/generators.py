import os, sys
script_path = os.path.join(os.path.dirname(__file__) , "..")
sys.path.append(script_path)

import preparation
import h5py as h5
import numpy as np
import keras as K

available_modes = {"train", "val"}
available_labels_encoding = {"hot", "smooth", False}


class HDF5NtupleGenerator(object):


  def __init__(
    self,
    src,
    group=".",
    X_key="x",
    y_key=[],
    sample_weights = None,
    n_data=-1,
    classes_key=None,
    batch_size=32,
    shuffle=True,
    scaler=None,
    num_classes=None,
    labels_encoding="hot",
    smooth_factor=0.1,
    mode="train"):

    self.mode = mode
    self.labels_encoding = labels_encoding
    
    if scaler is not None:
      if isinstance(scaler, preparation.Scaler):
        raise ValueError("`scaler` argument must be an instance of preparation.scaler")
    
    self.classes_key = classes_key
    self.batch_size = batch_size
    self.shuffle = shuffle
    self.scaler = scaler
    self.num_classes = num_classes
    self.smooth_factor = smooth_factor
    self.sample_weights = sample_weights
    self.metadata = None

    # Open global hdf5 file
    self.src = src
    self._hdf5 = h5.File(self.src, "r")

    if group:
      self._hdf5 = self._hdf5[group]

    # Get metadata associated with this file
    self.metadata = dict(self._hdf5.attrs)

    # Get reference to data
    self.X_key = X_key if isinstance(X_key, (list, tuple)) else [X_key]
    self.y_key = y_key


    print(">>>", tuple(self.X_key + [self.y_key]))
    # Initialize data
    if not self.sample_weights:
      self.data = self._hdf5[tuple(self.X_key + [self.y_key])]
    else:
      self.data = self._hdf5[tuple(self.X_key + [self.y_key] + [self.sample_weights])]

    # Get an index array
    _len = self.__get_dataset_shape(self.data[self.X_key[0]], 0)
    print("[INFO] The generator has the following lenth: %s" % _len)
    if (n_data != -1) and (_len > n_data):
      if n_data < self.batch_size:
        n_data = self.batch_size
      self._indices = np.random.choice(range(_len), int(n_data), replace=False)
    else:
      self._indices = np.arange(_len)

    # If the scaler is empty, fit to data
    if scaler is not None:
      if not scaler.loaded or not scaler.isFitted():
        self.fit_scaler()
      else:
        print("[INFO] Scaler has been loaded: %s" % self.scaler.fname)


  def __repr__(self):

    return self.__class__.__name__


  def __get_dataset_shape(self, dataset, index):

    return dataset.shape[index]


  def __get_dataset_items(self, indices, dataset = None):

      if dataset is not None:
        if not self.scaler:
          batch = np.array(self.data[indices][self.dnn.getVariables()].tolist())
        else:
          batch = np.array([np.array(self.scaler.transform(self.data[key][indices], key)) for key in dataset])
        return np.vstack(batch).T
      else:
        if not self.scaler:
          batch_x, batch_y = np.array(self.data[indices][self.dnn.getVariables()].tolist()), self.data[self.y_key][indices]
        else:
          batch_x, batch_y = np.array([self.scaler.transform(self.data[key][indices], key) for key in self.X_key]), self.data[self.y_key][indices]
        return np.vstack(batch_x).T, batch_y


  def get_xyw(self, preprocessed=True):

    print(self.X_key, self.y_key, self.sample_weights)

    x = self.get_entire_dataset(dataset=self.X_key, preprocessed=preprocessed)
    y = self.get_entire_dataset(dataset=self.y_key, preprocessed=False, shape=(-1,1))
    if self.sample_weights:
      w = self.get_entire_dataset(dataset=self.sample_weights, preprocessed=False, shape=(-1))
      return (x, y, w)
    return (x, y)


  def get_entire_dataset(self, dataset=None, preprocessed=True, shape=None):

    if not isinstance(dataset, list):
      dataset = [dataset]

    if not preprocessed or not self.scaler:
      if dataset: data = self.data[dataset][self._indices]
      else: data = self.data[self._indices]
    else:
      if not dataset: dataset = self.data.dtype.names
      data = [np.array(self.scaler.transform(self.data[key][self._indices], key)) for key in dataset]
      # Arrange
      data = np.vstack(data).T

    # If requested, reshape array
    if shape: data = data.reshape(shape)

    return np.array(data, dtype=np.float)


  @property
  def num_items(self):

    return self.data[self.X_key[0]].shape[0]


  @property 
  def classes(self):

    if self.classes_key is None:
      raise ValueError("Canceled. parameter `classes_key` "
                         "is set to None.")

    with h5.File(self.src, "r") as file:
      return file[self.classes_key][:]


  def __len__(self):

    return int(np.ceil(len(self._indices) / float(self.batch_size)))


  @staticmethod
  def apply_labels_smoothing(batch_y, factor):

    batch_y *= 1 - factor
    batch_y += factor / batch_y.shape[1]

    return batch_y


  def apply_labels_encoding(self, batch_y, smooth_factor = None):

    batch_y = K.utils.to_categorical(batch_y, num_classes=self.num_classes)

    if smooth_factor is not None:
      batch_y = self.apply_labels_smoothing(batch_y, factor=smooth_factor)
    return batch_y


  def fit_scaler(self):

    for key in self.X_key:
      self.scaler.fit(self.data[key][:], key)
    # Save
    self.scaler.save()
    print("[INFO] Scaler has been saved: %s" % self.scaler.fname)


  def __next_batch_test(self, indices):

    # Grab corresponding images from the HDF5 source file.
    batch_X = self.__get_dataset_items(indices, self.X_key)

    return batch_X


  def __next_batch(self, indices):

    # Grab samples (tensors, labels) HDF5 source file.
    (batch_X, batch_y) = self.__get_dataset_items(indices)

    # Shall we apply labels encoding?
    if self.labels_encoding:
      batch_y = self.apply_labels_encoding(
        batch_y,
        smooth_factor=self.smooth_factor
        if self.labels_encoding == "smooth" else None,
      )

    if not self.sample_weights:
      return (batch_X, batch_y)
    else:
      return (batch_X, batch_y, np.array(self.data[self.sample_weights][indices]))


  def __getitem__(self, index):

    # Indices for the current batch.
    indices = np.sort(self._indices[index * self.batch_size:(index + 1) * self.batch_size])

    if self.mode == "train":
      return self.__next_batch(indices)
    else:
      return self.__next_batch_test(indices)


  def __shuffle_indices(self):

    if (self.mode == "train") and self.shuffle:
      np.random.shuffle(self._indices)


  def keys(self):

    return self.X_key


  def on_epoch_end(self):

    self.__shuffle_indices()


if __name__ == "__main__":

  # generate toy data
  d1 = np.random.random(size = (1000,))
  d2 = 100*np.random.random(size = (1000,))
  d3 = np.random.random(size = (1000,))
  hf = h5.File("data.h5", "w")
  hf.create_dataset("x", data=d1)
  hf.create_dataset("z", data=d2)
  hf.create_dataset("y", data=d3)
  hf.close()

#  # Create a scaler
#  scaler = preparation.Scaler()
#  scaler.fit(d1, "x")

  # Load
  train_gen = HDF5NtupleGenerator(
    src="data.h5",
    X_key=["x"],
    y_key="y",
    n_data=1,
    scaler=None,
    labels_encoding="hot",
    batch_size=8)

 # for x in train_gen: print(x)

  print(len(train_gen))
