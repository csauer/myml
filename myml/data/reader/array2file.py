import os, sys
import numpy as np
import myutils.h5


def array2root (arr, branches, path2file="output.root", tree_name="EventTree", mode="update", scaler=None):

  from root_numpy import array2root

  # Convert array to structures arrayp
  arr_rec = np.core.records.fromrecords(arr, names=branches)
 
  # Undo preprocessing?
  if scaler != None:
    for key in inpt.dtype.names:
      inpt[key] = scaler.transform(inpt[key], key, inverse=True)

  # To ROOT file 
  array2root(arr_rec, path2file, treename=tree_name, mode=mode)


def recarray2root (arr, path2file="output.root", tree_name="EventTree", mode="update", scaler=None):

  from root_numpy import array2root

  # Undo preprocessing?
  if scaler != None:
    for key in arr.dtype.names:
      arr[key] = scaler.inverseTransform(arr[key], key)

  # To ROOT file 
  array2root(arr, path2file, treename=tree_name, mode=mode)
