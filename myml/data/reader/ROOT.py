import os, sys
import numpy as np
import h5py
import collections


__branches_in_file__ = None


def load_batch(path2file, tree_name, inputs=[], targets=[], weights=[],
  batch_size=None, max_size=0, preprocessor=None, randomize=True, verbose=False):

  if inputs and not isinstance(inputs, list):
    inputs = [inputs]
  if targets and not isinstance(targets, list):
    targets = [targets]
  if weights and not isinstance(weights, list):
    weights = [weights]

  if __branches_in_file__ is None:
    t_f = ROOT.TFile(path2file, "READ")
    t_t = f.Get(tree_name)
    global __branches_in_file__
    __branches_in_file__ = [for key in t_t.GetListOfKeys()]
    t__f.Close()

  # Some checks on inputs
  if not inputs and not targets:
    inputs = __branches_in_file__
  elif not inputs and targets:
    inputs = list(__branches_in_file__)
    for col in targets+weights:
      inputs.remove(col)

  # Get the datasets
  datasets = collections.OrderedDict()
  for col in inputs+targets+weights:
    datasets[col] = f[col]

  # Get size of datasets
  size = set([ds.len() for ds in datasets.itervalues()])
  if len(size) > 1:
    print("[ERROR] The different datasets have diffeent sizes! Use smalles common value.")
  size = min(size)
  store_size = size

  if max_size != 0:
    size = min(max_size,size)
    if verbose: print("Limited size to " + str(size))

  if batch_size is None:
    batch_size = size

  # Shuffle the data?
  if randomize:
    subset = np.sort(np.random.choice(store_size,replace=False, size=(size//batch_size,batch_size)))

  i_start, step = 0, 0
  while True:

    if size >= i_start+batch_size:

      if randomize:
        mask = subset[i_start//batch_size]
      else:
        mask = range(i_start, i_start+batch_size)

      result = []
      result.append([])
      for col in inputs:
        ds = datasets[col][mask]
        if preprocessor is not None:
          ds = preprocessor(ds, col)
        result[-1].append(ds)
      if targets:
        result.append([])
        for col in targets:   
          ds = datasets[col][mask]
          if preprocessor is not None:
            ds = preprocessor(ds, col)
          result[-1].append(ds)
      if weights:
        result.append([])
        for col in weights:   
          ds = datasets[col][mask]
          if preprocessor is not None:
            ds = preprocessor(ds, col)
          result[-1].append(ds)
      if len(result) == 1:
        yield result[0]
      else:
        yield tuple(result)
      i_start += batch_size
      step += 1
    else:
      i_start = 0


if __name__ == "__main__":

  for x in load_batch("data.h5", inputs=["x", "y"], batch_size=2):
  #for x in load_batch("data.h5", inputs="z", batch_size=1):
    print(x)
