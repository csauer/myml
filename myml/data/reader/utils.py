import sys
import time


class state (object):


  def __init__ (self):

    # State variables
    self.epoch = 0
    self.batch = 0
    self.it = 0
    # For time measurement
    self.time = [0, 0]


  def __repr__(self):
    
    dt = 0
    # Start time measure
    if self.time[0] == 0:
      self.time[0] = time.time()
    elif self.time[0] != 0 and self.time[1] == 0:
      self.time[1] = time.time()
      dt = self.time[1] - self.time[0]
      self.time = [self.time[1], 0]
    
    return "[PROGRESS] dt %0.05fs - Epoch %s/%s - Batch %s/%s - It. %s/%s" % (dt, self.epoch, self.n_epochs, self.batch, self.n_batches, self.it, self.n_it_tot)


  def __add__(self, x=1):

    # Increase number of iterations
    self.it += x
    self.update()
    return self


  def setNtotNepochsBs(self, n_tot, n_epochs, batch_size):

    self.n_epochs = n_epochs
    self.batch_size = batch_size
    self.n_tot = n_tot
    if self.n_tot >= self.batch_size:
      self.n_batches = self.n_tot // self.batch_size
    else:
      print("[ERROR] There must be more number of points in the sample than the size of one batch")
      sys.exit()
    self.n_it_tot = self.n_batches * self.n_epochs


  def update (self):

    self.epoch = int(self.it*self.batch_size/self.n_tot)
    self.batch = int(self.it % self.n_batches)


  def isNewEpoch (self):

    if self.batch == 0: return True
    else: return False
