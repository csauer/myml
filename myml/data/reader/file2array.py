import os, sys
import numpy as np
import myutils.h5


def _not_found(keys_available, keys_requested):

  if isinstance(keys_requested, list):
    if not all(key in keys_available  for key in keys_requested):
      print("[ERROR] Not all requested keys (%s) are in HDF5 file" % keys_requested)
      print("        Choose between: %s" % ", ".join(keys_available))
      sys.exit()
  elif isinstance(keys_requested, str):
    if keys_requested not in keys_available:
      print("[ERROR] Not all requested keys (%s) are in HDF5 file" % keys_requested)
      print("        Choose between: %s" % ", ".join(keys_available))
      sys.exit()


def hdf2array (path2file, inputs, targets=[], ds_name=None, n=-1, selection=None, sample_weight=[], class_weight=[], scaler=None, shuffle=True, to_array=True):

  import h5py
  with h5py.File(path2file, "r") as f:
 
    if ds_name != None and ds_name in f.keys():
      ds = f[ds_name]
    else:
      ds = f

    # Make a preselection before loading everything into memory
    if not isinstance(inputs, list): inputs = [inputs]
    if not isinstance(targets, list): targets = [targets]
    if not isinstance(sample_weight, list): sample_weight = [sample_weight]
    if not isinstance(class_weight, list): class_weight = [class_weight]
    keys_all =  list(inputs)
    if targets:
      keys_all += targets
    if sample_weight:
      keys_all += sample_weight
    if class_weight:
      keys_all += class_weight
    # Also check for keys that are requested in selection
    if selection != None:
      for key in ds.dtype.names:
        if key in selection and key not in keys_all:
          keys_all.append(key)
    # Get data with fields
    ds = ds[tuple(keys_all)]

    # TODO: Change thos later!!!

    # Load all data into memory
#    ds = ds[:]
    # Reduce number of events
    if n != -1 and n < ds.shape[0]:
      ds = ds[0:n]


    # If requested, apply a selection on entries
    if selection != None:
      if not isinstance(selection, list):
        ds = myutils.h5.rootlike_selection(ds, selection=selection)

    # Tuple/list with the arrays to be returned
    r_tuple = [] 
    
    # Actual inputs
    _not_found(ds.dtype.names, inputs)
    inpt = ds[inputs]
    n_events = inpt.shape[0]
    # Preprocess the dataset?
    if scaler != None:
      # Fit scaler to dataset and transform data
      for key in inpt.dtype.names:
        # If the scaler has already been fitted to data, fit won't have any effect
        scaler.fit(inpt[key], key)
        # Apply preprocessing
        inpt[key] = scaler.transform(inpt[key], key)
    r_tuple.append(inpt)
    # Targets
    if targets:
      _not_found(ds.dtype.names, targets)
      r_tuple.append(ds[targets])
    # Sample weights
    if sample_weight:
      _not_found(ds.dtype.names, sample_weight)
      r_tuple.append(ds[sample_weight])
    # Classes
    if class_weight:
      _not_found(ds.dtype.names, class_weights)
      r_tuple.append(ds[class_weight])

    if to_array:
      # Convert to normal numpy arrays
      for i in range(len(r_tuple)):
        r_tuple[i] = np.array(r_tuple[i].tolist())

    if shuffle:
      # Get array of permuted indices
      idx = np.random.permutation(n_events)
      for i in range(len(r_tuple)):
        r_tuple[i] = r_tuple[i][idx]

    if len(r_tuple) > 1:
      return tuple(r_tuple)
    else:
      return r_tuple[0]
