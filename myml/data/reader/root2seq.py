import os, sys

# Scientific imports
import ROOT
import numpy as np
import sklearn.utils

# Get module path
import utils
script_path = os.path.join(os.path.dirname(__file__) , "..")
sys.path.append(script_path)
import preparation


class SeqDataFromROOT (ROOT.TMVA.DataLoader):


  def __init__(self, analysis_type="classification", n_max=10):

    # Call base class constructor
    super(SeqDataFromROOT, self).__init__(ROOT.TString("TmvaBased/"))
    # Initialize some member variables
    self.analysis_type = analysis_type
    # Dictionary for indices for test and training set
    self.idx = {"training":None, "test":None}
    # Which mode are we in? Train or test?
    self.mode = "training"
    # Arrays that hold data
    self.data_in_memory = { "training": None, "test": None, "validation": None } 
    # We need a state for Training and Test data
    self.state = { "training": utils.state(), "test": utils.state() }
    # Processor of the data
    self.processor = None
    # Use one-hop encoeed labels?
    self.one_hop = True
    # Max lenght of time sequence
    self.n_max = n_max
    # Dictionary that holds information for variables
    self.var_info = {}
    self.dtypes = []
    # List cuts
    self.common_cuts = []


  def __processEvent(self, x, y, w, event):

    # Get event weight
    w = event.GetWeight()
    # Get values
    for j in range(self.GetDataSetInfo().GetNVariables()):
      x[j] = event.GetValue(j)
      if x[j] == self.var_info[j]["default"]:
        x[j] = self.var_info[j]["padding"]
    # Labels
    if self.analysis_type == "classification":
      self.one_hot = True
      y[event.GetClass()] = 1
    else:
      # In case of regression problem
      for j in range(self.GetDataSetInfo().GetNClasses()):
        y[j] = event.GetTarget(j)


  def __update_idx (self):

    # Number of events
    n_train = self.GetDataSetInfo().GetDataSet().GetNTrainingEvents()
    n_test = self.GetDataSetInfo().GetDataSet().GetNTestEvents()
    # Index arrays
    self.idx["training"] = np.arange(n_train)
    self.idx["test"] = np.arange(n_test)


  def addDataset (self, path2files, treename, classname, weight=1.0, cut=""):

    if len(self.getListOfVariables()) == 0:
      print("[ERROR] List of variables are not defined. Call SeqDataFromROOT.addVariable()")
      sys.exit()

    # Some checks on input
    if not isinstance(path2files, list): path2files = [path2files]

    # Add (merge) root files to TChains
    t_tree_tmp = ROOT.TChain(treename)
    for f in path2files: t_tree_tmp.Add(f)

    # Check if the class name already exists
    if classname in self.getListOfClasses():
      print("[WARNING] The class `%s` is already defined! Data is added to existing class" % classname)

    # Update cut
    cut_update = "&&".join(self.common_cuts + [cut])
    if cut_update != "":
      print("[INFO] The following cut will be used for dataset (%s): %s" % (classname, cut_update))

    # Add tree to data loader
    self.AddTree(t_tree_tmp, ROOT.TString(classname), weight, ROOT.TCut(cut_update))

    # Update number of events
    self.__update_idx()


  def addVariable(self, varname, varexp="", title="", unit="", dtype="F", min=0, max=0, pad=0, default=-999):

    if varname not in self.dtypes:
      self.dtypes.append(varname)

    if varexp == "":
      varexp = "%s[__replace__]" % varname
    else:
      varexp = varexp.replace(varname, "%s[__replace__]" % varname)

    # Add variable to TMVA
    for i in range(self.n_max):
      name = "%s.At(__replace__) := %s" % (varname, varexp)
      self.AddVariable(ROOT.TString(name.replace("__replace__", str(i))), ROOT.TString(title), ROOT.TString(unit), dtype, min, max)
      # Add information to variable lookup
      self.var_info[self.getNvariables()-1] = {"name":varname, "expression":varexp, "padding":0, "default":default}


  def addCommonCut (self, cut, delimiter="&&"):

    if not isinstance(cut, list):
      cut = cut.replace(" ", "").split(delimiter)

    # Update common cuts 
    self.common_cuts = cut


  def getNclasses (self):

    return self.GetDataSetInfo().GetNClasses()


  def getListOfClasses (self):

    return [self.GetDataSetInfo().GetClassInfo(i_c).GetName() for i_c in range(self.getNclasses())]


  def getListOfVariables(self, as_list=False):

    if not as_list:
      return self.GetDataSetInfo().GetListOfVariables()
    else:
      return [str(var) for var in self.GetDataSetInfo().GetListOfVariables()]


  def getCut (self, classname):

    return self.GetDataSetInfo().GetCut(classname).GetTitle()


  def getNvariables (self):

    return len(self.getListOfVariables(as_list=True))


  def getNin (self):

    return int (self.GetDataSetInfo().GetNVariables())


  def getNout (self):

    return int(self.GetDataSetInfo().GetNClasses())


  def getEntireDataSet(self, mode=None, processed=True):

    if mode == None: mode = self.mode.title()

    n = getattr(self.GetDataSetInfo().GetDataSet(), "GetN%sEvents" % mode)()
    n_in  = self.getNin() 
    n_out = self.getNout()

    # Numpy array for data with correct shapes
    x, y, w = np.zeros((n, n_in)), np.zeros((n, n_out)), np.ones((n,))

    # Fill containers
    for i in range(n):
      e = getattr(self.GetDataSetInfo().GetDataSet(), "Get%sEvent" % mode)(i)
      self.__processEvent(x[i], y[i], w[i], e)

    # Cast to numpy array for simple slicing
    x, y, w = np.reshape(np.asarray(x), ((n, n_in//self.n_max, self.n_max))), np.asarray(y), np.asarray(w)
    # Make x a structured array
    if self.processor != None and processed:
      for pos, name in  enumerate(self.dtypes):
        x[:,pos] = self.processor.transform(x[:, pos], name)
   
    # Suffle dataset
    p = np.random.permutation(len(x))
    x, y, w = x[p], y[p], w[p]

    # Return batch of (suffled) data
    return (x, y, w)


  def getBatch(self, shapes=None, verbose=1, processed=True, shuffle=True):

    # Is this the beginning of a new epoch
    if self.state["training"].isNewEpoch():
      # Read data into memory (will be shuffled)
      self.data_in_memory[self.mode] = self.getEntireDataSet()
      if verbose == 1: print(self.state["training"])
     
    # Get some informations about the state
    bs, batch = self.state["training"].batch_size, self.state["training"].batch

    # Split datset into components
    x, y, w = self.data_in_memory[self.mode]
    # Shuffle?
    if shuffle: x, y, w = sklearn.utils.shuffle(x, y, w)

    # Update state machine
    self.state["training"] += 1
    if verbose == 2: print(self.state["training"])

    return (x[batch*bs:(batch+1)*bs], y[batch*bs:(batch+1)*bs], w[batch*bs:(batch+1)*bs])


  def prepareTrainAndTestTree(self, n_train, n_test, batch_size=1, n_epochs=10, classname=None):

    n_train_list, n_test_list = [], []
    if classname == None:
      # Aplly to all classes
      for c_name in self.getListOfClasses():
        n_train_list.append("nTrain_%s=%d" % (c_name, n_train))
        n_test_list.append("nTest_%s=%d" % (c_name, n_test))
    else:
      n_train_list.append("nTrain_%s=%d" % (classname, n_train))
      n_test_list.append("nTest_%s=%d" % (classname, n_test))

    # Update state
    self.prepare_string = ":".join(n_train_list+n_test_list) + ":VerboseLevel=Info"
    self.PrepareTrainingAndTestTree(ROOT.TCut(""), self.prepare_string)
    print("[INFO] PrepareTrainingAndTestTree('', %s)" % ":".join(n_train_list+n_test_list))

    # Update number of events
    self.__update_idx()

    # Update state
    n_train = self.GetDataSetInfo().GetDataSet().GetNTrainingEvents()
    n_test = self.GetDataSetInfo().GetDataSet().GetNTestEvents()
    # Set state for training and testing
    self.state["training"].setNtotNepochsBs(n_train, n_epochs, batch_size)
    self.state["test"].setNtotNepochsBs(n_test, n_epochs, batch_size)
    

  def setProcessor(self, processor=None, method="id", fname=None):

    assert isinstance(processor, type(preparation.Scaler)) or processor==None, "Type of processor not understood"

    # Get the entire /training) dataset (not yet transformed)
    x, _, _ = self.getEntireDataSet(mode="Training")

    if processor == None and fname != None:
      print("[INFO] Creating a new scaler for preprocessing of data")
      self.processor = preparation.Scaler(fname, method)
    if processor != None:
      self.processor = processor

    # Check if this processor has already been fitted to data
    if not self.processor.isFitted():
      print("[INFO] The procesor passed has not been fitted to data yet. This will be done now.")
      # Combine all variables with same name to one array
      for pos, name in  enumerate(self.dtypes):
        self.processor.fit(x[:,pos], name)
      # Save this scaler
      self.processor.save()
    print("[INFO] A processor has been given. All data will be preprocessed.")

