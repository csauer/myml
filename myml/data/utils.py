import numpy as np

def shuffle_array(arr):

  if not isinstance(arr, list): arr = [arr]  
  idx = np.random.permutation(len(arr[0]))
  for i in range(len(arr)):
    arr[i] = arr[i][idx]

  return arr
