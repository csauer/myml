import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, "../../.."))


def _mkdirs(path):
  dirname = os.path.dirname(path)
  if not os.path.exists(dirname):
    os.makedirs(dirname)


def get_parameter(model):

  import numpy as np
  l_weights_all, l_weights, l_biases = [], [], []
  # Get all weighst
  for layer in model.layers:
    a = layer.get_weights()
    # Check if empty
    if len(a) == 0:
      continue
    if len(a) !=0:
      for l in a:
        for w in l.flatten(): # list of numpy arrays
          l_weights_all.append(w)
      for w in a[0].flatten():
        l_weights.append(w)
      if 2 == len(a):
        for w in a[1].flatten():
          l_biases.append(w)
  return np.asarray(l_weights_all), np.asarray(l_weights), np.asarray(l_biases)


def load_model(directory, fname=None, custom_layers=None):

  from keras.models import load_model, model_from_json

  if os.path.isdir(directory):
    path2file = os.path.join(directory, fname)
    if not path2file.endswith(".json"):
      model = load_model(path2file)
    else:
      json_file = open(path2file, "r")
      loaded_model_json = json_file.read()
      model = model_from_json(loaded_model_json, custom_objects=custom_layers)
      json_file.close()
    print("[ML HELP - INFO] Loaded model: %s" % path2file)
    return model
  elif os.path.isfile(directory):
    if not directory.endswith(".json"):
      model = load_model(directory, custom_objects=custom_layers)
    else:
      json_file = open(directory, "r")
      loaded_model_json = json_file.read()
      json_file.close()
      model = model_from_json(loaded_model_json, custom_objects=custom_layers)
    print("[ML HELP - INFO] Loaded model: %s" % directory)
    return model


def load_models(path2models, custom_layers=None, weight_pattern="weight"):

  from keras.models import load_model, model_from_json
  import glob


def load_weight(model, directory, name=None, options="epoch=9999"):

  import ntpath, myutils.name
  # Load weight files
  if name:
    model.load_weights(os.path.join(directory, name))
    print("ML HELP - INFO: Loaded model: %s" % os.path.join(directory, name))
    return

  if os.path.isfile(directory):
    model.load_weights(directory)
    return
  
  # Else, get latest or best weight configuration
  weight_list = [os.path.join(directory, fname) for fname in os.listdir(directory) if fname.endswith(".h5")]
  # Get number(s) from weight name
  epochs = [int(myutils.name.decode(ntpath.basename(wname), tag="ep")) for wname in weight_list if "ep_" in wname]
  # File
  fname = weight_list[epochs.index(max(epochs))]
  # Load model
  model.load_weights(fname)
  print("[ML HELP - INFO] Loaded model weights: %s" % weight_list[epochs.index(max(epochs))])
  return fname


def save_config (model, path2model, use_name=True, save_weights=True, save_model=True):

  # Make sure that all directories exist
  _mkdirs(path2model)

  if use_name:
    if not isinstance(model, list):
      model = [model]
    for m in model:
      # Save weights
      if save_weights:
        m.save_weights(os.path.join(path2model, "%s.weights.h5" % m.name))
      if save_model:
        # Save model
        with open(os.path.join(path2model, "%s.model.json" % m.name), "w") as f:
          f.write(m.to_json())
  else:
      # Save weights
      path2model = path2model.replace(".h5", "").replace(".json", "")
      head, tail = os.path.split(path2model)
      if save_weights:
        model.save_weights(os.path.join(head, "weight.%s.h5" % tail))
      if save_model:
        # Save model
        if tail != "":
          with open(os.path.join(head, "model.%s.json" % tail), "w") as f:
            f.write(model.to_json())
        else:
          with open(os.path.join(head, "model.json"), "w") as f:
            f.write(model.to_json())
 

def save_diagram (model, path2diagram, use_name=True, show_shapes=True):

  # Make sure that all directories exist
  _mkdirs(path2diagram)

  from keras.utils.vis_utils import plot_model
  if use_name:
    if not isinstance(model, list):
      model = [model]
    for m in model:
      # Save weights
      plot_model(m, os.path.join(path2diagram, "%s.png" % m.name), show_shapes=show_shapes)
  else:
      # Save weights
      plot_model(model, path2diagram+".png", show_shapes=show_shapes)
 

def model_name(model):
  pass


if __name__ == "__main__":

  import keras.backend as K
  from keras.models import Sequential
  from keras.layers import Dense
  
  model = Sequential()
  model.add(Dense(1, input_shape=(1,)))
  model.add(Dense(1))
  model.compile(loss="mse", optimizer="adam")
  print(dir(model))
  print(K.eval(model.count_params))
